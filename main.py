#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/11/2 下午 04:56
# @Author : Aries
# @Site : 
# @File : main.py
# @Software: PyCharm


from FindDog import Picture

if __name__ == "__main__":
    pictures = Picture(r'DogPicture')
    # 新增圖片標籤
    # pictures.add_tag(r'AddPicture')
    # 以圖搜圖
    #　pictures.pictures_find(r'AddPicture\不可以色色.jpg')
    # 關鍵字搜尋圖片
    pictures.key_find('inu')
