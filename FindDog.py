#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/11/2 下午 05:00
# @Author : Aries
# @Site : 
# @File : Google.py
# @Software: PyCharm

import os
import cv2
import time
import datetime
import pymysql
import numpy as np
import shutil
from selenium import webdriver
import matplotlib.pyplot as plt
import matplotlib.image as img


class Picture:
    def __init__(self, folder_path):
        self.host = '120.101.8.51'
        self.user = 'dog'
        self.passwd = 'dog'
        self.db = 'dog'
        self.charset = 'utf8'
        self.folder_path = os.getcwd() + '/' + folder_path

    def add_tag(self, folder_path):
        # 找到tag然後回傳
        def find_tag(dr,fp,pn):
            try:
                dr.find_element_by_class_name('ZaFQO').click()
                dr.find_element_by_link_text('上傳圖片').click()
                dr.find_element_by_id('awyMjb').send_keys(os.getcwd() + f'/{fp}/' + pn)
                time.sleep(1)
                f_tag = driver.find_element_by_name('q').get_attribute('value')
            except:
                time.sleep(2)
                dr.refresh()
                time.sleep(5)
                f_tag = find_tag(dr,fp,pn)

            return f_tag

        # 連接資料庫
        conn = pymysql.connect(host=self.host, user=self.user, passwd=self.passwd, database=self.db,  charset=self.charset)
        cursor = conn.cursor()

        print('開始取得標籤......')
        start_time = datetime.datetime.now()
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        driver = webdriver.Chrome(options=options)

        # 新增cookie不然會被bang
        # driver.get('https://www.google.com/imghp?hl=zh-TW')
        # cookie = {'name': 'GOOGLE_ABUSE_EXEMPTION',
        #           'value': 'ID=19eba90f2af47b3c:TM=1636017375:C=r:IP=120.101.8.134-:S=Qvb2kNjRAywIhuk8rV7oNYU',
        #           'domain': '.google.com', 'path': '/'}
        # driver.add_cookie(cookie)
        # driver.refresh()

        for picture_name in os.listdir(folder_path):
            driver.get('https://www.google.com/imghp?hl=zh-TW')
            time.sleep(1)
            tag = find_tag(driver,folder_path,picture_name)
            # 如果被bang就跳開
            if 'EgR4ZQiGG' in tag:
                break
            cursor.execute(f"""insert into pictures(name,tag) values("{picture_name}","{tag}")""")
            conn.commit()
            # 移到完成的資料夾
            shutil.move(os.getcwd() + f'/{folder_path}/' + picture_name, self.folder_path + '/' + picture_name)

        end_time = datetime.datetime.now()
        conn.close()
        print('標籤取得完成 耗時: ' + str(end_time - start_time))



    def pictures_find(self, picture):
        def getHash(image):
            avreage = np.mean(image)
            hash = []
            for i in range(image.shape[0]):
                for j in range(image.shape[1]):
                    if image[i, j] > avreage:
                        hash.append(1)
                    else:
                        hash.append(0)
            return hash

        def Hamming_distance(hash1, hash2):
            num = 0
            for index in range(len(hash1)):
                if hash1[index] != hash2[index]:
                    num += 1
            return num

        def classify_aHash(image1, image2):
            image1 = cv2.resize(image1, (8, 8))
            image2 = cv2.resize(image2, (8, 8))
            gray1 = cv2.cvtColor(image1, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
            hash1 = getHash(gray1)
            hash2 = getHash(gray2)
            return Hamming_distance(hash1, hash2)

        start_time = datetime.datetime.now()

        conn = pymysql.connect(host=self.host, user=self.user, passwd=self.passwd, database=self.db, charset=self.charset)
        cursor = conn.cursor()
        cursor.execute('select name from pictures')
        img_arr = np.array(cursor.fetchall())

        # 平均哈希算法
        aHash = []
        for img_index in range(0, len(img_arr[:,0])):
            img1 = img.imread(self.folder_path + '/' + img_arr[img_index, 0])
            img2 = img.imread(picture)
            aHash.append(classify_aHash(img1, img2))

        end_time = datetime.datetime.now()
        print('搜尋完成 耗時: ' + str(end_time - start_time))

        # 取出最小的10筆
        for i in range(10):
            index = aHash.index(min(aHash))
            aHash.pop(index)
            image = img.imread(self.folder_path + '/' + img_arr[index, 0])
            plt.imshow(image)
            plt.show()



    def key_find(self, target_key):
        start_time = datetime.datetime.now()
        conn = pymysql.connect(host=self.host, user=self.user, passwd=self.passwd, database=self.db, charset=self.charset)
        cursor = conn.cursor()

        cursor.execute(f"select name from pictures where tag like '%{target_key}%'")
        name_arr = np.array(cursor.fetchall())

        end_time = datetime.datetime.now()
        print('搜尋完成 耗時: ' + str(end_time - start_time))

        if len(name_arr) != 0:
            for name_index in range(0, len(name_arr[:,0])):
                image = img.imread(self.folder_path + '/' + name_arr[name_index, 0])
                plt.imshow(image)
                plt.show()
        else:
            print('無搜尋結果')

